
<!-- README.md is generated from README.Rmd. Please edit that file -->

# mesoutils

<!-- badges: start -->
<!-- badges: end -->

L’objectif de mesoutils est de rassembler un ensemble de fonctions
permettant de decrire des jeux de donnees

## Installation

La version de developpement de mesoutils peut être installe de la
maniere suivante :

``` r
install.packages("mesoutils")
```

## Example

Voici quelques exemples de fonctionnement des fonctions que contient le
package mesoutils :

``` r
library("mesoutils")

data("my_dataset")

get_info_data(my_dataset)
#> $dimension
#> [1] 35 14
#> 
#> $names
#>  [1] "name"       "height"     "mass"       "hair_color" "skin_color"
#>  [6] "eye_color"  "birth_year" "sex"        "gender"     "homeworld" 
#> [11] "species"    "films"      "vehicles"   "starships"
get_mean_data(data = my_dataset)
#> # A tibble: 1 × 3
#>   height  mass birth_year
#>    <dbl> <dbl>      <dbl>
#> 1   177.  82.8       53.4
```
